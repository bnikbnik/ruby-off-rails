require "./controllers/base_controller"

class HomeController < BaseController
  def index
    @data = Item.all

    @title = "Welcome to our Ruby Off Rails homepage!"

    response render_html("home")
  end

  def hello
    @name = params["name"]

    response render_html("hello")
  end

  def json
    response *render_json({ this: "is", a: "great", thing: "!" })
  end
end
