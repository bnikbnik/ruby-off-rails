require "./controllers/base_controller"

class FunController < BaseController
  def static_page
    @title = "This is a static page."

    response render_html("static_page", layout: "alt_layout")
  end
end
