class BaseController
  BASE_VIEW_PATH = "./views"

  attr_accessor :params

  def initialize(params)
    @parmas = params
  end

  def response(content, options={})
    options[:status]  ||= "200"
    options[:headers] ||= { "Content-Type" => "text/html" }

    [ options[:status], options[:headers], [content] ]
  end

  def not_found
    puts "Server couldn't find the specified page. PATH_INFO: #{params["path"]}"
    response "Sorry, this page doesn't exist", { status: "404" }
  end

  def render_html(view, layout: "layout")
    layout_file = File.read(File.join(BASE_VIEW_PATH, "#{layout}.haml"))
    view_file   = File.read(File.join(BASE_VIEW_PATH, "#{view}.haml"))

    locals = {}
    instance_variables.each do |var|
      locals[var] = instance_variable_get(var)
    end

    Haml::Engine.new(layout_file).render(Object.new, locals) do
      Haml::Engine.new(view_file).render(Object.new, locals)
    end
  end

  def render_json(json)
    [ json.to_json, { headers: { "Content-Type" => "text/json" } } ]
  end
end
