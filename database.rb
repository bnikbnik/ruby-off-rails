class Database
  def self.config
    {
      host: "localhost",
      user: "psql_user",
      password: 1234
    }
  end

  def self.dbname
    "rack_app_dev"
  end

  def self.connect
    Sequel.postgres(dbname, config)
  end

  def self.create
    conn = Sequel.postgres("postgres", config)

    puts "Creating database #{dbname}..."
    conn.execute("CREATE DATABASE #{dbname}")
    puts "Done!"
  rescue PG::ConnectionBad => e
    $stderr.puts e
  rescue Sequel::DatabaseError => e
    if e.cause.class == PG::DuplicateDatabase
      $stderr.puts "Database #{dbname} already exists."
    else
      $stderr.puts e
    end
  end

  def self.setup
    puts "Connecting to #{dbname}..."
    conn = Sequel.postgres(dbname, config)

    puts "Creating tables etc..."

    conn.create_table :items do
      primary_key :id
      String :name
      Float :price
    end

    puts "Done!"
  rescue PG::ConnectionBad => e
    $stderr.puts e
  rescue Sequel::DatabaseError => e
    if e.cause.class == PG::DuplicateTable
      $stderr.puts e.message
    else
      $stderr.puts e
    end
  end
end
