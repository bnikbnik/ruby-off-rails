# README #

Ruby off Rails is a dead simple web application in ruby using just Rack without Rails or any other frameworks.

It was born out of my curiosity about how rack works, and how a ruby web framework like rails would be created.

# Setup and Run #

```
bundle
ruby app.rb run db:create
ruby app.rb run db:setup
ruby app.rb start
```

Note that currently proper migration handling for the database isn't supported yet, database connectivity and models are WIP.
