require "rubygems"
require "bundler/setup"
Bundler.require(:default)

def production?
  ENV["APP_ENV"] == "production"
end

def development?
  !production? || ENV["APP_ENV"] == "development"
end

require "./models/item"
require "./controllers/home_controller"
require "./controllers/fun_controller"
require "./database"

def run_server
  rack_app = Rack::Builder.new do
    puts "-- Server is running in production mode -- " if production?
    puts "-- Server is running in development mode -- " if development?

    if development?
      use Rack::ShowExceptions
      use Rack::CommonLogger
      use Rack::Lint
    end

    app = proc do |env|
      params = env["QUERY_STRING"].split("&").map{|e| e.split("=")}.to_h
      params["path"] = env["PATH_INFO"]

      case params["path"]
      when "/"
        HomeController.new(params).index
      when "/hello"
        HomeController.new(params).hello
      when "/api"
        HomeController.new(params).json
      when "/static_page"
        FunController.new(params).static_page
      else
        HomeController.new(params).not_found
      end
    end

    run app
  end.to_app

  Rack::Server.start app: rack_app, server: :webrick, Port: 3000
end

case ARGV[0]
when "start"
  run_server
when "run"
  case ARGV[1]
  when "db:create"
    Database.create
  when "db:setup"
    Database.setup
  else
    puts "Not sure what to do!"
  end
when "console"
  # TODO HACK It works, but it's not really an isolated console environment
  binding.pry
else
  puts "Goodbye! :)"
end
