class BaseModel
  attr_accessor :attributes

  def initialize(data)
    @db = Database.connect
    @table = @db[self.class.table_name]
    @attributes = {}

    @table.columns.each do |col|
      @attributes[col] = data[col]

      self.class.send(:define_method, col) do
        @attributes[col]
      end

      self.class.send(:define_method, "#{col}=") do |value|
        @attributes[col] = value
      end
    end
  end

  def inspect
    "#<#{self.class}:#{object_id} #{@attributes}>"
  end

  def save
    # TODO This should update if it exists
    @table.insert(@attributes)
  end

  # TODO We need some way of checking persistance with the models. For example
  # self.all creates new model objects, but these should be understood by the
  # app as persisted and save shouldn't try to re-insert them.
  #
  def self.all
    @db = Database.connect
    @table = @db[self.table_name]

    @table.map do |row|
      self.new(row)
    end
  end

  def self.table_name
    self.to_s.downcase.to_sym
  end
end
